import h5py
import numpy as np

f=h5py.File('demo.h5','w')

f['set1'] = np.random.randn(18*12).reshape((18,12))
for i in xrange(0,3):
    g='grp'+str(i)
    f.create_group(g)
    f.create_group('grp0/sub'+g)
for i in range(0,2):
    g='grp'+str(i)
    g2='grp0/subgrp'+str(i)
    f[g]['x'+str(i)]=np.arange(10)
    f[g]['y'+str(i)]=np.random.randn(10)
    f[g2]['x'+str(i)]=np.arange(10)
    f[g2]['y'+str(i)]=np.random.randn(10)
f['set1'].attrs['info']='this is basic'
f['set1'].attrs['status']='uninteresting'
f.attrs['info']='This is a demo file'
f['grp0/subgrp1'].attrs['test']='subgroup attribute called test'
f['grp0/subgrp2'].attrs['integer']=2

f.close()
