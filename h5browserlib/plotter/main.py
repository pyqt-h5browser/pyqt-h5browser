from __future__ import print_function

import matplotlib as mpl
mpl.rcParams['backend']= 'Qt4Agg'
from matplotlib import pyplot as plt

##
# Plots one dataset
# @warning for 2d-dataset this is quite slow
def simple(h5obj,dset):
    print("Plotting set:",dset)
    h5set=h5obj[dset]
    #plt.title(dset)
    if len(h5set.shape)==1:
        plt.xlabel('Index')
        plt.ylabel('Value')
        plt.plot(h5set[:])
    elif len(h5set.shape)==2 and h5set.shape[0]==1 or h5set.shape[1]==1:
        plt.xlabel('Index')
        plt.ylabel('Value')
        plt.plot(h5set[:])
    elif len(h5set.shape)==2: 
        plt.xlabel('Index 1')
        plt.ylabel('Index 2')
        cs=plt.contourf(h5set[:])
        plt.colorbar(cs).ax.set_ylabel('Value')
    else:
        return
    plt.show()
    
