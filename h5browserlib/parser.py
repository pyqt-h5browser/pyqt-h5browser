
#parser:
import optparse,sys,os



def get_parse_args():
    # process the command line arguments and options
    usage = '%prog [options] <hdf5file>'
    parser = optparse.OptionParser(usage)
    optargs = parser.parse_args()
    
    # checking that input is sane:
    if len(sys.argv)!=2:
        parser.error("Wrong input")
    else:
        if not os.path.isfile(sys.argv[1]):
            parser.error('Not a valid file path')
    
    return optargs