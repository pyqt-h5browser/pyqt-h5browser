# -*- coding: utf-8 -*-

"""The user interface for our app"""

from . import browser,parser

# Import Qt modules
from PyQt4 import QtGui

import os,sys


def main():
    (options,args) = parser.get_parse_args()
    
    # Loading our browser window and showing it:
    app = QtGui.QApplication(sys.argv)
    window=browser(sys.argv[1])
    window.show()
    
    # It's exec_ because exec is a reserved word in Python
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
