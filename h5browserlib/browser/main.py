from __future__ import print_function

# Import Qt modules
from PyQt4 import QtCore,QtGui

# Import the compiled UI module
from windowUi import Ui_MainWindow

# import backend:
import h5py

from h5browserlib import plotter

# Create a class for our main window
class Main(QtGui.QMainWindow):
    def __init__(self,h5file):
        QtGui.QMainWindow.__init__(self)

        # This is always the same
        self.ui=Ui_MainWindow()
        self.ui.setupUi(self)
        self.db=h5py.File(h5file,'r')
        # Insert groups into the list..
        for g in self.db:
            self.addObject(g,'/')
    
    def addObject(self,name,parent,parentwidget=None):
        if 'Group' in str(type(self.db[parent+name])):
            self.addGroup(name,parent,parentwidget)
        elif 'Dataset' in str(type(self.db[parent+name])):
            self.addDataset(name,parent,parentwidget)
    
    def addGroup(self,name,parent,parentwidget=None):
        item=QtGui.QTreeWidgetItem([parent+name])
        if parentwidget==None:
            self.ui.list.addTopLevelItem(item)
        else:
            parentwidget.addChild(item)
        item.type='group'
        ttip=self.getAttrs(parent+name)
        if ttip:
            item.setToolTip(1,ttip)
        item.parent=parent
        item.name=name
        if item.checkState(0):
            item.setExpanded(True)
        for e in self.db[parent+name]:
            self.addObject(e,parent+name+'/',item)
    
    def addDataset(self,name,parent,parentwidget=None):
        item=QtGui.QTreeWidgetItem([parent,name])
        item.setCheckState(0,QtCore.Qt.Unchecked)
        item.type='dataset'
        ttip=self.getAttrs(parent+name)
        if ttip:
            item.setToolTip(1,ttip)
        item.parent=parent
        item.name=name
        if parentwidget==None:
            self.ui.list.addTopLevelItem(item)
        else:
            parentwidget.addChild(item)
    
    def getAttrs(self,name):
        attrs=''
        for attr in self.db[name].attrs:
            attrs+=' '+attr+": "+str(self.db[name].attrs[attr])+"\n"
        if attrs:
            attrs= "Attributes:\n"+attrs
        if hasattr(self.db[name],'shape'):
            attrs= "Shape: "+str(self.db[name].shape)+"\n"+attrs
        return attrs
    
    ##
    # @todo figure out why this is called twice??
    def on_actionStart_plotter_triggered(self):
        print("You selected to start a plotter window")
    
    ##
    # @todo figure out why this is called twice??
    def on_actionSelect_file_triggered(self):
        print("You selected to open file selection dialog")
    
    ##
    # Currently simply plot the data in new window..
    def on_list_itemChanged(self,item,column):
        if not hasattr(item,"name"):
            return
        if item.checkState(0):
            un=''
        else:
            un='un'
        
        if item.type=='dataset':
            print("You "+un+"selected variable",item.name,'from',item.parent)
        else:
            print("You "+un+"selected an unknown type",item.name)
            
        if item.checkState(0):
            plotter.simple(self.db,item.parent+item.name)
            
