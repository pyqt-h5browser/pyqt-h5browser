# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'window.ui'
#
# Created: Thu Aug 11 17:52:46 2011
#      by: PyQt4 UI code generator 4.8.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(508, 514)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.horizontalLayout = QtGui.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.list = QtGui.QTreeWidget(self.centralwidget)
        self.list.setEnabled(True)
        self.list.setMinimumSize(QtCore.QSize(500, 0))
        self.list.setObjectName(_fromUtf8("list"))
        self.list.header().setDefaultSectionSize(250)
        self.list.header().setStretchLastSection(False)
        self.horizontalLayout.addWidget(self.list)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 508, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuThis_is_some_type = QtGui.QMenu(self.menubar)
        self.menuThis_is_some_type.setObjectName(_fromUtf8("menuThis_is_some_type"))
        self.menuHelp = QtGui.QMenu(self.menubar)
        self.menuHelp.setObjectName(_fromUtf8("menuHelp"))
        MainWindow.setMenuBar(self.menubar)
        self.actionSelect_file = QtGui.QAction(MainWindow)
        self.actionSelect_file.setObjectName(_fromUtf8("actionSelect_file"))
        self.actionStart_plotter = QtGui.QAction(MainWindow)
        self.actionStart_plotter.setObjectName(_fromUtf8("actionStart_plotter"))
        self.actionAbout = QtGui.QAction(MainWindow)
        self.actionAbout.setObjectName(_fromUtf8("actionAbout"))
        self.menuThis_is_some_type.addAction(self.actionSelect_file)
        self.menuThis_is_some_type.addAction(self.actionStart_plotter)
        self.menuHelp.addAction(self.actionAbout)
        self.menubar.addAction(self.menuThis_is_some_type.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "HDF5 Browser", None, QtGui.QApplication.UnicodeUTF8))
        self.list.headerItem().setText(0, QtGui.QApplication.translate("MainWindow", "Group", None, QtGui.QApplication.UnicodeUTF8))
        self.list.headerItem().setText(1, QtGui.QApplication.translate("MainWindow", "Dataset", None, QtGui.QApplication.UnicodeUTF8))
        self.menuThis_is_some_type.setTitle(QtGui.QApplication.translate("MainWindow", "Tools", None, QtGui.QApplication.UnicodeUTF8))
        self.menuHelp.setTitle(QtGui.QApplication.translate("MainWindow", "Help", None, QtGui.QApplication.UnicodeUTF8))
        self.actionSelect_file.setText(QtGui.QApplication.translate("MainWindow", "Select file", None, QtGui.QApplication.UnicodeUTF8))
        self.actionStart_plotter.setText(QtGui.QApplication.translate("MainWindow", "Start plotter", None, QtGui.QApplication.UnicodeUTF8))
        self.actionAbout.setText(QtGui.QApplication.translate("MainWindow", "About", None, QtGui.QApplication.UnicodeUTF8))

