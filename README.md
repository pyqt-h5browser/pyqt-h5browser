# pyqt-h5browser

This project aims to create a simple GUI browser for
HDF5 files, using PyQt and matplotlib

## Unmaintained

This project never reached a satisfactory maturity, and now the author found a similar project
which is more feature complete at https://github.com/ganymede42/h5pyViewer
The author warmly recommend to have a look at that instead.

## Installation

Install PyQt4, Python setup tools, and then run the command

```
pip install git+https://gitlab.com/pyqt-h5browser/pyqt-h5browser.git
```

## Running

```
h5browser my_file.h5
```

## License
License: GPL v. 3 or later
