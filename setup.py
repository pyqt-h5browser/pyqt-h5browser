#!/usr/bin/python

from setuptools import setup, find_packages

setup(name='h5browser',
    version='0.3',
    description='Python library browsing and plotting data from HDF5 files',
    author='Yngve Inntjore Levinsen',
    author_email='Yngve.Levinsen[at]gmail..com',
    license='GPLv3',
    url='https://gitlab.com/pyqt-h5browser',
    packages=find_packages(),
    install_requires=['h5py'],
    entry_points={
        'console_scripts': [
            'h5browser=h5browserlib.h5browser:main',
        ],
    }
    )

